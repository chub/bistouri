# -*- coding: utf-8 -*-
from irc3.plugins.command import command
import irc3
import time
import datetime
import dateparser
from pony.orm import *

db = Database()

class Message(db.Entity):
	ts = PrimaryKey(int)
	nick = Required(str)
	channel = Required(str)
	message = Required(str)


class Presence(db.Entity):
	nick = Required(str)
	channel = Required(str)
	login = Required(int)
	logout = Required(int)
	present = Required(bool)
	PrimaryKey(nick, channel)

@irc3.plugin
class Plugin:

	def __init__(self, bot):
		self.bot = bot
		db.bind('sqlite', 'bistouri.sqlite', create_db=True)
		db.generate_mapping(create_tables=True)

	@irc3.event(irc3.rfc.RPL_NAMREPLY)
	@db_session
	def namereply(self, channel=None, data=None, **kw):
		"""list active user in the channel"""
		now = int(time.time())
		for nick in data.split(' '):
			if nick.startswith("@") or nick.startswith("+"):
				nick = nick[1:]
			try:
				presence = Presence[nick, channel]
				#we assume that user that where already
				#logged in didn't disconnect meawhile
				if not presence.present:
					presence.login = now
					presence.present = True
			except ObjectNotFound:
				presence = Presence(
					nick = nick,
					channel = channel,
					login=now,
					logout=now,
					present = True
				)
		commit()


	@irc3.event(irc3.rfc.JOIN)
	@db_session
	def user_join(self, mask, channel, **kw):
		"""register when someone join a channel"""
		now = int(time.time())
		if mask.nick == self.bot.nick:
			return
		try:
			presence = Presence[mask.nick, channel]
			presence.login = now
			presence.present = True
		except ObjectNotFound:
			presence = Presence(
				nick = mask.nick,
				channel = channel,
				login=now,
				logout=now,
				present = True
			)
		commit()

	@irc3.event(irc3.rfc.QUIT)
	@db_session
	def user_quit(self, mask, **kw):
		"""register when someone leave a channel"""
		now = int(time.time())
		if mask.nick == self.bot.nick:
			return
		#update user on every connected channels
		for p in Presence.select(lambda p: p.nick == mask.nick):
			p.logout = now
			p.present = False
		commit()

	@irc3.event(irc3.rfc.PART)
	@db_session
	def user_part(self, mask, channel, **kw):
		"""register when someone leave a channel"""
		now = int(time.time())
		if mask.nick == self.bot.nick:
			return
		try:
			presence = Presence[mask.nick, channel]
			presence.logout = now
			presence.present = False
		except ObjectNotFound:
			presence = Presence(
				nick = mask.nick,
				channel = channel,
				login=now,
				logout=now,
				present = False
			)
		commit()

	@irc3.event(irc3.rfc.PRIVMSG)
	@db_session
	def user_message(self, mask, target, data, **kw):
		"""Say hi when someone join a channel"""
		now = int(time.time())
		if mask.nick == self.bot.nick:
			return
		Message(
			ts=now,
			nick = mask.nick,
			channel = target,
			message = data
		)
		commit()

	@command(permission="view", public=False)
	def help(self, mask, target, args):
		"""Help

		    %%help
		"""
		pass

	@command(permission='view', public=False)
	@db_session
	def update(self, mask, target, args):
		"""Update

			%%update <channel>
		"""
		try:
			presence = Presence[mask.nick, args["<channel>"]]
		except ObjectNotFound:
			yield "{} is not watched or you never logged in".format(args["<channel>"])
			return
		yield "replaying messages from {} to {}".format(
			datetime.datetime.fromtimestamp(presence.logout),
			datetime.datetime.fromtimestamp(presence.login)
			)
		if presence.present:
			for m in Message.select(lambda m: m.ts > presence.logout
									and m.ts < presence.login
									and m.channel == presence.channel):
				yield "{} <{}> {}".format(
					datetime.datetime.fromtimestamp(m.ts),
					m.nick,
					m.message)
		else:
			for m in Message.select(lambda m: m.ts > presence.logout
									and m.channel == presence.channel):
				yield "{} <{}> {}".format(
					datetime.datetime.fromtimestamp(m.ts),
					m.nick,
					m.message)


	@command(permission='view', public=False)
	@db_session
	def log(self, mask, target, args):
		"""Log

			%%log <channel> since <since>...
		"""
		channel = args["<channel>"]
		since = dateparser.parse(" ".join(args["<since>"]))
		if since is None:
			yield "'{}' is not a valid point in time".format(
				" ".join(args["<since>"]))
			return
		since_ts = since.timestamp()
		yield "messages on {} since {}".format(channel, since)
		for m in Message.select(lambda m: m.ts > since_ts and m.channel == channel):
			yield "{} <{}> {}".format(
				datetime.datetime.fromtimestamp(m.ts),
				m.nick,
				m.message)
